# Installation

1 - Install **lxd**

sudo apt install -y lxd

2 - Launch **lxd** configuration script and follow the instructions

sudo lxd init

3 - Add **user** to **lxd** group if it is not already done

sudo adduser [user] lxd
