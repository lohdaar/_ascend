<?php
// src/AppBundle/Controller/AM_MainController.php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AM_MainController extends Controller
{
    /**
     * @Route("/home")
     */
     public function numberAction()
     {
         $number = mt_rand(0, 100);

         return $this->render('home.html.twig', array(
             'number' => $number,
         ));
      }
}
