# Project Arkhadia
## What is it ?

**Arkhadia-NetSys** is a personnal network composed of different assets to suit my needs in developpement and sysadmin.

## Project details

**REDO THE DETAILS**

OS:

- Ubuntu Server 18.04

Assets:

- A Firewall (What kind ? Which tech ?)
- A DMZ w/ :
  + 1 Server for DNS, DHCP and other services
  + 1 Server for data storage purposes (Database management)
  + 1 Server [name] for critical services (HashiCorp Open Source Products)
- 1 to 5 clients for test purposes (outside the net.project network)

**[SRV - Name Server]**

- NSD
- DHCP (Only for clients)

**[SRV - Database Server]**

**[SRV - Main Server]**

### TODO
Install in priority:

- LXD

Scheduled:

- HashiCorp Vagrant (Virtualization)
- HashiCorp Packer (Machine Images Automation)
- HashiCorp Consul (Service Discovery, Configuration and Monitoring)
- HashiCorp Vault (Secrets Management Tool)

## Naming
### For Servers

- arkh-[server purpose]
  + arkh-dns, arkh-ssh, arkh-postgresql, ...
  + arkh-vagrant, arkh-packer, arkh-consul, arkh-vault, arkh-docks, ...

### For Clients

- arkh
  + arkh-kali, arkh-ubuntu, arkh-slackware, arkh-gentoo, ...

### For Network

The network name will be ```arkhadia.net```.
